# Easily connect to Ivanti Connect Secure (formerly "Pulse Secure") VPN via `openconnect`

This Firefox addon makes it easier to connect to your company's Ivanti Connect Secure VPN
without the need for the client application - just use `openconnect`!

With the addon installed, simply visit `https://vpn.yourcompany.com/smd`.
After signing in, you'll find a helpful section on the page with an `openconnect` command
that is ready for copy-and-pasting into your Terminal.

![Screenshot of the addon in action](screenshot.png?raw=true)

## How to package the addon

1. Install `web-ext` (see [their GitHub README][web-ext])
2. From your checkout, run `web-ext build`

## Source code checklist (Firefox addon review)

- [x] **Did you use any build tools?** No.
- [x] **Does your package include source code for any private repositories or frameworks used in your add-on?** All of the code is open source. It does not contain any frameworks.
- [x] **Operation system used for the build:** macOS 11.1
- [x] **Details of any specific versions of tools or utilities needed:** There are none. A simple text editor will suffice.
- [x] **Links to any tools or utilities that need to be downloaded:** n/a
- [x] **Guidance for installing any downloaded tools and utilities, for example, links to online instructions:** n/a
- [x] **instructions for building your add-on code or details of any scripts provided:** n/a
- [x] **Does your package include your build script?** I do not have a build script. The instructions on how to build are part of this README.

## Attributions

The icon is taken from the Google Material Design iconset, and is used under the terms of the
[Creative Commons Attribution-ShareAlike](https://creativecommons.org/licenses/by-sa/3.0/) license.

## License

This addon is published under the [Mozilla Public License, version 2.0][license].

[web-ext]: https://github.com/mozilla/web-ext
[license]: https://www.mozilla.org/en-US/MPL/2.0/
