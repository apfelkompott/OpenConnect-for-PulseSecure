let allLinkHrefs = Array.from(document.getElementsByTagName('link')).map(element => element.href);
let isIvantiConnectSecurePage = (typeof allLinkHrefs.find(src => src.indexOf('/dana-cached/build/manifest.json') != -1)) !== 'undefined';

function createCardElement(openconnectCommand) {
  let card = document.createElement('div');
  card.classList.add('card');

  let container = document.createElement('div');
  container.classList.add('container');
  card.appendChild(container);

  let text = document.createElement('div');
  text.classList.add('text');
  container.appendChild(text);

  let code = document.createElement('code');
  code.style.fontFamily = 'Courier New';
  code.appendChild(document.createTextNode('openconnect'));

  text.appendChild(document.createTextNode('Connect using '));
  text.appendChild(code);
  text.appendChild(document.createTextNode(': '));

  let input = document.createElement('input');
  input.style.width = '100%';
  input.value = openconnectCommand;
  text.appendChild(input);

  let copyTextNode = document.createTextNode('Copy');
  let copiedTextNode = document.createTextNode('Copied ✔');

  let copyButton = document.createElement('button');
  copyButton.classList.add('brcd-btn');
  copyButton.classList.add('brcd-btn--blue');
  copyButton.classList.add('brcd-waves-effect');
  copyButton.onclick = function () {
    navigator.clipboard.writeText(openconnectCommand).then(
      () => {
        copyButton.classList.remove('brcd-btn--blue');
        copyButton.classList.add('brcd-btn--green');
        copyButton.removeChild(copyTextNode);
        copyButton.appendChild(copiedTextNode);
      },
      () => {
        copyButton.classList.remove('brcd-btn--blue');
        copyButton.classList.add('brcd-btn--red');
      });
  };
  copyButton.appendChild(copyTextNode);
  container.appendChild(copyButton);

  return card;
}

function setupOpenconnectHelper() {
  // Since the page is a React app and will take some time to load, we need to work with an interval here.
  var addCardInterval;
  var didAddCardElement = false;
  function attemptToAddCardElement() {
    if (didAddCardElement) {
      return;
    }

    let cardsContainer = document.getElementsByClassName('pulseClientCard')[0];
    if (!cardsContainer) {
      return;
    } else {
      // Stop the interval immediately.
      didAddCardElement = true;
      clearInterval(addCardInterval);

      let dsid = document.cookie.split(";").find((item) => item.includes("DSID=")).trim().replace('DSID=', '');
      let command = `sudo openconnect --protocol=nc https://${document.location.host}/smd -C DSID=${dsid}`;

      cardsContainer.appendChild(createCardElement(command));
    }
  }

  addCardInterval = setInterval(attemptToAddCardElement, 500);
}

if (isIvantiConnectSecurePage) {
  setupOpenconnectHelper();
}
